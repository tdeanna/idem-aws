# Remove all dependencies of a vpc, the vpc_id comes from params
{% set vpc_id = params["vpc_id"] %}

orphan-internet-gateways:
  exec.run:
    - path: aws.ec2.internet_gateway.list
    - kwargs:
        name: null
        filters:
          - name: "attachment.vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-subnets
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-internet-gateways}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.internet_gateways.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-subnets:
  exec.run:
    - path: aws.ec2.subnet.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-subnets
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-subnets}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.subnet.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-route-tables:
  exec.run:
    - path: aws.ec2.subnet.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-route-tables
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-route-tables}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.route_table.absent:
    - resource_id: {{ resource['resource_id'] }}
  {% endfor %}
#!END

orphan-network-acls:
  exec.run:
    - path: aws.ec2.network_acl.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-network-acls
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-network-acls}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.network_acl.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-vpc-peering-connections:
  exec.run:
    - path: aws.ec2.vpc_peering_connection.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-vpc-peering-connections
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-vpc-peering-connections}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.vpc_peering_connection.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-vpc-endpoints:
  exec.run:
    - path: aws.ec2.vpc_endpoint.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-vpc-peering-connections
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-vpc-endpoints}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.vpc_endpoint.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-nat-gateways:
  exec.run:
    - path: aws.ec2.nat_gateway.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-nat-gateways
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-nat-gateways}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.nat_gateway.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-security-groups:
  exec.run:
    - path: aws.ec2.security_group.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-security-groups
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-security-groups}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.security_group.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-instances:
  exec.run:
    - path: aws.ec2.instance.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-instances
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-instances}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.instance.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-vpc-connections:
  exec.run:
    - path: aws.ec2.vpc_connection.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-instances
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-vpc-connections}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.vpc_connection.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-vpn-gateways:
  exec.run:
    - path: aws.ec2.vpn_gateway.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-vpn-gateways
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-vpn-gateways}') or () %}
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.vpn_gateway.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-network-interfaces:
  exec.run:
    - path: aws.ec2.network_interface.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-vpn-gateways
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-network-interfaces}') or () %}
  cleanup-{{ resource['resource_id'] }}:
  aws.ec2.network_interface.absent:
    - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

orphan-carrier-gateways:
  exec.run:
    - path: aws.ec2.carrier_gateway.list
    - kwargs:
        name: null
        filters:
          - name: "vpc-id"
            values:
              - {{ vpc_id }}

#!require:orphan-carrier-gateways
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan-carrier-gateways}') or () %}
  cleanup-{{ resource['resource_id'] }}:
    aws.ec2.carrier_gateway.absent:
      - resource_id: {{ resource['resource_id'] }}
{% endfor %}
#!END

# Last of all, remove the vpc
cleanup-vpc:
  aws.ec2.vpc.absent:
    - resource_id: {{ vpc_id }}
