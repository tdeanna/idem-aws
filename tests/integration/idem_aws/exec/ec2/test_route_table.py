import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_route_table):
    get_name = "idem-test-exec-get-route-table-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.route_table.get(
        ctx,
        name=get_name,
        resource_id=aws_ec2_route_table["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert get_name == resource.get("name")
    assert aws_ec2_route_table["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    get_name = "idem-test-exec-get-route-table-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.route_table.get(
        ctx,
        name=get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.route_table '{get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_route_table):
    list_name = "idem-test-exec-list-route-table-" + str(int(time.time()))
    filters = [
        {"name": "route-table-id", "values": [aws_ec2_route_table["resource_id"]]}
    ]
    ret = await hub.exec.aws.ec2.route_table.list(
        ctx,
        name=list_name,
        filters=filters,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert aws_ec2_route_table["resource_id"] == resource.get("resource_id")
