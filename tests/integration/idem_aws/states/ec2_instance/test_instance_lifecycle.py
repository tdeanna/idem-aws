"""Use this test class for testing (custom) reconciliation only"""
import json
import tempfile

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_ec2_instance_e2e_lifecycle(
    hub,
    idem_cli,
    acct_profile,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    ctx,
):
    """
    It had to use two separate SLS files for validating is_pending
    """
    create_instance_sls_content = """
    create_aws_ec2_instance:
      aws.ec2.instance.present:
        - name: {}
        - instance_type: {}
        - image_id: {}
        - tags:
            Name: {}
    """.format(
        instance_name,
        aws_ec2_instance_type["resource_id"],
        aws_ec2_ami_pro["resource_id"],
        instance_name,
    )

    resource_id = None
    with tempfile.NamedTemporaryFile(
        mode="wb+", prefix="ec2_instance_create_", suffix=".sls", delete=True
    ) as ec2_instance_create_sls:
        ec2_instance_create_sls.write(create_instance_sls_content.encode())
        ec2_instance_create_sls.flush()
        ret = idem_cli(
            "state",
            ec2_instance_create_sls.name,
            "--reconciler=basic",
            "--pending=aws",
            "--output=json",
            f"--acct-profile={acct_profile}",
        )
        assert ret.result is True
        output = json.loads(ret.stdout)
        assert output
        ec2_instance = next(v for k, v in output.items() if "aws.ec2.instance" in k)
        assert ec2_instance
        assert ec2_instance.get("new_state")

        resource_id = ec2_instance["new_state"].get("resource_id", None)
        assert resource_id

        running = ec2_instance["new_state"].get("running", False)
        assert running

        instance_state = ec2_instance["new_state"].get("instance_state", None)
        assert instance_state == "running"

        # Verify that we can perform a successful "get" and it is running
        get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=resource_id)

        assert get.result
        assert get.ret
        assert get.ret["resource_id"] == resource_id
        assert get.ret.get("running")
        assert get.ret.get("instance_state", None) == "running"

    assert resource_id is not None

    delete_instance_sls_content = """
        delete-aws_ec2_instance:
          aws.ec2.instance.absent:
            - resource_id: {}
        """.format(
        resource_id
    )

    with tempfile.NamedTemporaryFile(
        mode="wb+", prefix="ec2_instance_delete_", suffix=".sls", delete=True
    ) as ec2_instance_delete_sls:
        ec2_instance_delete_sls.write(delete_instance_sls_content.encode())
        ec2_instance_delete_sls.flush()
        ret = idem_cli(
            "state",
            ec2_instance_delete_sls.name,
            "--reconciler=basic",
            "--pending=aws",
            "--output=json",
            f"--acct-profile={acct_profile}",
        )
        assert ret.result is True
        output = json.loads(ret.stdout)
        assert output
        ec2_instance = next(v for k, v in output.items() if "aws.ec2.instance" in k)
        assert ec2_instance
        assert not ec2_instance.get("new_state")

        # Verify that we can perform a successful "get" and it is NOT running and terminated
        get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=resource_id)
        assert get.result
        assert get.ret
        assert not get.ret.get("running")
        assert get.ret.get("instance_state", None) == "terminated"


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_ec2_instance_e2e_with_instance_stopped_when_provisioned(
    hub,
    idem_cli,
    acct_profile,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    ctx,
):
    """
    It had to use two separate SLS files for validating is_pending
    """
    create_instance_sls_content = """
    create_aws_ec2_instance:
      aws.ec2.instance.present:
        - name: {}
        - instance_type: {}
        - image_id: {}
        - running: False
        - tags:
            Name: {}
    """.format(
        instance_name,
        aws_ec2_instance_type["resource_id"],
        aws_ec2_ami_pro["resource_id"],
        instance_name,
    )

    resource_id = None
    with tempfile.NamedTemporaryFile(
        mode="wb+", prefix="ec2_instance_create_", suffix=".sls", delete=True
    ) as ec2_instance_create_sls:
        ec2_instance_create_sls.write(create_instance_sls_content.encode())
        ec2_instance_create_sls.flush()
        ret = idem_cli(
            "state",
            ec2_instance_create_sls.name,
            "--reconciler=basic",
            "--pending=aws",
            "--output=json",
            f"--acct-profile={acct_profile}",
        )
        assert ret.result is True
        output = json.loads(ret.stdout)
        assert output
        ec2_instance = next(v for k, v in output.items() if "aws.ec2.instance" in k)
        assert ec2_instance
        assert ec2_instance.get("new_state")

        resource_id = ec2_instance["new_state"].get("resource_id", None)
        assert resource_id

        running = ec2_instance["new_state"].get("running", False)
        assert not running

        instance_state = ec2_instance["new_state"].get("instance_state", None)
        assert instance_state == "stopped"

        # Verify that we can perform a successful "get" and it is running
        get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=resource_id)

        assert get.result
        assert get.ret
        assert get.ret["resource_id"] == resource_id
        assert not get.ret.get("running")
        assert get.ret.get("instance_state", None) == "stopped"

    assert resource_id is not None

    delete_instance_sls_content = """
        delete-aws_ec2_instance:
          aws.ec2.instance.absent:
            - resource_id: {}
        """.format(
        resource_id
    )

    with tempfile.NamedTemporaryFile(
        mode="wb+", prefix="ec2_instance_delete_", suffix=".sls", delete=True
    ) as ec2_instance_delete_sls:
        ec2_instance_delete_sls.write(delete_instance_sls_content.encode())
        ec2_instance_delete_sls.flush()
        ret = idem_cli(
            "state",
            ec2_instance_delete_sls.name,
            "--reconciler=basic",
            "--pending=aws",
            "--output=json",
            f"--acct-profile={acct_profile}",
        )
        assert ret.result is True
        output = json.loads(ret.stdout)
        assert output
        ec2_instance = next(v for k, v in output.items() if "aws.ec2.instance" in k)
        assert ec2_instance
        assert not ec2_instance.get("new_state")

        # Verify that we can perform a successful "get" and it is NOT running and terminated
        get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=resource_id)
        assert get.result
        assert get.ret
        assert not get.ret.get("running")
        assert get.ret.get("instance_state", None) == "terminated"
