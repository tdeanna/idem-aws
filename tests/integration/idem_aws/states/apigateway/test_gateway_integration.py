import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource-" + str(int(time.time())),
    "http_method": "GET",
    "input_type": "AWS_PROXY",
    "passthrough_behavior": "WHEN_NO_MATCH",
    "timeout_in_millis": 29000,
    "integration_http_method": "ANY",
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(
    hub, ctx, __test, aws_apigateway_method, aws_lambda_function, cleanup
):
    global PARAMETER
    rest_api_id = aws_apigateway_method.get("rest_api_id")
    parent_resource_id = aws_apigateway_method.get("parent_resource_id")
    PARAMETER["http_method"]
    PARAMETER["parent_resource_id"] = parent_resource_id
    PARAMETER["rest_api_id"] = rest_api_id
    PARAMETER["uri"] = hub.tool.aws.arn_utils.build(
        service="apigateway",
        region=ctx["acct"]["region_name"],
        account_id="lambda",
        resource="path/2015-03-31/functions/"
        + aws_lambda_function["function_arn"]
        + "/invocations",
    )

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.integration.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    PARAMETER["cache_namespace"] = resource.get("cache_namespace")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.integration", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.integration", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["input_type"] == resource.get("input_type")
    assert PARAMETER["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert PARAMETER["timeout_in_millis"] == resource.get("timeout_in_millis")
    print("URI", __test, resource.get("uri"))
    assert PARAMETER["uri"] == resource.get("uri")
    # integration_http_method is not returned by AWS. so asserting only in case of real aws.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["integration_http_method"] == resource.get(
            "integration_http_method"
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.integration.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.integration.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.apigateway.integration.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["input_type"] == described_resource_map.get("input_type")
    assert PARAMETER["cache_namespace"] == described_resource_map.get("cache_namespace")
    assert PARAMETER["passthrough_behavior"] == described_resource_map.get(
        "passthrough_behavior"
    )
    assert PARAMETER["http_method"] == described_resource_map.get("http_method")
    # integration_http_method is not returned by AWS. so asserting only in case of real aws.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["integration_http_method"] == described_resource_map.get(
            "integration_http_method"
        )
    assert PARAMETER["parent_resource_id"] == described_resource_map.get(
        "parent_resource_id"
    )
    assert PARAMETER["rest_api_id"] == described_resource_map.get("rest_api_id")
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.integration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["input_type"] == resource.get("input_type")
    assert PARAMETER["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert PARAMETER["timeout_in_millis"] == resource.get("timeout_in_millis")
    assert PARAMETER["cache_namespace"] == resource.get("cache_namespace")
    # integration_http_method is not returned by AWS. so asserting only in case of real aws.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["integration_http_method"] == resource.get(
            "integration_http_method"
        )


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-invalid-resource-id", depends=["present"])
async def test_get_invalid_resource_id(hub, ctx):
    resource_id = "fake-id"
    ret = await hub.exec.aws.apigateway.integration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"] is False, ret["comment"]
    assert ret["ret"] is None
    assert f"Invalid Resource ID '{resource_id}'." in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["present"])
async def test_get_resource_id_does_not_exist(hub, ctx):
    resource_id = "restApiId-resourceId-httpMethod"
    ret = await hub.exec.aws.apigateway.integration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get '{resource_id}' result is empty" and f"NotFoundException" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    updated_passthrough_behavior = "NEVER"
    new_parameter["passthrough_behavior"] = updated_passthrough_behavior

    ret = await hub.states.aws.apigateway.integration.present(ctx, **new_parameter)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.integration", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert new_parameter["resource_id"] == resource.get("resource_id")
        assert new_parameter["rest_api_id"] == resource.get("rest_api_id")
        assert new_parameter["parent_resource_id"] == resource.get("parent_resource_id")
        assert new_parameter["http_method"] == resource.get("http_method")
        assert new_parameter["name"] == resource.get("name")
        assert new_parameter["passthrough_behavior"] == resource.get(
            "passthrough_behavior"
        )
        assert new_parameter["timeout_in_millis"] == resource.get("timeout_in_millis")
        assert new_parameter["cache_namespace"] == resource.get("cache_namespace")
        assert new_parameter["input_type"] == resource.get("input_type")
        # integration_http_method is not returned by AWS. so asserting only in case of real aws.
        if not hub.tool.utils.is_running_localstack(ctx):
            assert new_parameter["integration_http_method"] == resource.get(
                "integration_http_method"
            )
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.apigateway.integration", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )

        if not __test:
            PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["present"])
async def test_absent_integration(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.integration.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["passthrough_behavior"] == resource.get("passthrough_behavior")
    assert PARAMETER["timeout_in_millis"] == resource.get("timeout_in_millis")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.integration",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.integration",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.integration.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.integration",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_integration_absent_with_none_resource_id(hub, ctx):
    "idem-method-integration-" + str(int(time.time()))
    # Delete method response with resource_id as None. Result in no-op.
    ret = await hub.states.aws.apigateway.integration.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.integration",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.integration.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
